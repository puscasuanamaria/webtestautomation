package origo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class AccountPage extends PageObject {

    @FindBy(id="customer_logout_link")
    private WebElementFacade logoutButton;

    /**
     * Get text from xpath.
     * @return login name.
     */
    public String get_login_identificator() {
        return find(By.xpath("/html/body/div[3]/div[2]/p")).getText();
    }

    /**
     * Click logout button
     */
    public void click_logout_button() {
        logoutButton.click();
    }

    /**
     * Get text from xpath.
     * @return - success message - ready to login
     */
    public String get_logout_identificator() {
        return find(By.xpath("/html/body/div[3]/div[2]/p")).getText();
    }
}

