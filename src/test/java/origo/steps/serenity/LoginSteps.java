package origo.steps.serenity;

import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import origo.pages.AccountPage;
import origo.pages.HomePagePage;
import origo.pages.LoginPage;

import static junit.framework.TestCase.assertTrue;

public class LoginSteps {
    HomePagePage homePagePage;
    LoginPage loginPage;
    AccountPage accountPage;

    /**
     * Open main page https://origocoffee.ro.
     */
    @Step
    public void is_the_home_page() {
        homePagePage.open();
    }

    /**
     * Click  - open login page- meniu option: "cont".
     */
    @Step
    public void click_login_page() {
        homePagePage.click_login_page();
    }

    /**
     * Open login page.
     * Validation: should see message "LOG IN".
     * @param text - message "LOG IN".
     */
    @Step
    public void should_see_valid_login_text(String text) {
        Assert.assertEquals(loginPage.get_login_identificator(), text);
    }

    /**
     * Add login param.
     * @param email
     * @param pass
     */
    @Step
    public void enters_data_and_click_login(String email, String pass) {
        loginPage.enter_email(email);
        loginPage.enter_password(pass);
        loginPage.click_signin_button();
    }

    /**
     * Open account page.
     * Validation: should see message "CONT".
     * @param text - message "CONT".
     */
    @Step
    public void should_see_valid_account_text(String text) {
        assertTrue(accountPage.get_login_identificator().contains(text));
    }
}
