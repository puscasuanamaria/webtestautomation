package origo.steps.serenity;

import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import origo.pages.*;

import static junit.framework.TestCase.assertTrue;

public class AddToBagSteps {
    SearchPage searchPage;
    ProductPage productPage;
    BagPage bagPage;
    HomePagePage homePagePage;
    ShopPage shopPage;
    CategoryPage categoryPage;

    /**
     * Open main page https://origocoffee.ro.
     */
    @Step
    public void is_the_home_page() {
        homePagePage.open();
    }

    /**
     * Click on the "visit our shop".
     * Result: Open shop page.
     */
    @Step
    public void go_shopping(){
        homePagePage.visit_shop();
    }

    /**
     * Shop page should be opened.
     * Validation: should see a specific message.
     * @param text -  shop identification
     */
    public void should_see_valid_shop_text(String text) {
        Assert.assertEquals(shopPage.get_shop_identificator(), text);
    }

    /**
     * Click on the category product.
     * Result: open product category page.
     */
    @Step
    public void select_product_type(){
        shopPage.select_product_type();
    }

    /**
     * Category product page should be opened.
     * Validation: should see a specific message.
     * @param text - product category identification
     */
    public void should_see_valid_product_category_text(String text) {
        Assert.assertEquals(categoryPage.get_category_identificator(), text);
    }

    /**
     * Search by product name.
     * Result: Should open product page.
     * @param productName - search term
     */
    @Step
    public void select_product(String productName){
        categoryPage.select_product(productName);
    }

    /**
     * Product page should be opened.
     * Validation: should see success message.
     * @param text - product identification.
     */
    public void should_see_valid_product_text(String text) {
        Assert.assertEquals(productPage.get_product_identificator(), text);
    }

    /**
     * Add product in bag - set units and quantity
     * @param units
     * @param quantity
     */
    @Step
    public void set_units_set_quantity_and_click_collect(String units, String quantity){
        productPage.set_units(units);
        productPage.click_quantity();
//        productPage.select_quantity();
        productPage.click_collect();
    }

    /**
     * Bag page should be opened.
     * Validation: should see success message.
     * @param text - bag identification.
     */
    @Step
    public void should_see_text(String text) {
        assertTrue(bagPage.get_bag_identificator().contains(text));
    }

    /**
     * Search the product in bag.
     * @param text -product name.
     */
    @Step
    public void should_find_correct_product_in_bag(String text) {
        assertTrue(bagPage.check_element_by_attribute_value_product(text));
    }

    /**
     * Search the product in bag.
     * @param text - product units.
     */
    @Step
    public void should_find_correct_units_in_bag(String text) {
        assertTrue(bagPage.check_element_by_attribute_value_units("value", text));
    }

}
