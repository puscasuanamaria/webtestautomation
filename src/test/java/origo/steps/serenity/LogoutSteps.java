package origo.steps.serenity;

import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import origo.pages.AccountPage;
import origo.pages.HomePagePage;
import origo.pages.LoginPage;

public class LogoutSteps {
    HomePagePage homePagePage;
    AccountPage accountPage;
    LoginPage loginPage;

    /**
     * Open main page https://origocoffee.ro.
     */
    @Step
    public void is_the_home_page() {
        homePagePage.open();
    }

    /**
     * Click  - open login page- meniu option: "cont".
     */
    @Step
    public void click_login_page() {
        homePagePage.click_login_page();
    }

    /**
     * Open account page.
     * Validation: should see login name.
     * @param text - login name.
     */
    @Step
    public void should_see_valid_login_text(String text) {
        Assert.assertEquals(accountPage.get_login_identificator(), text);
    }

    /**
     * Click logout button.
     * Result: return to home page.
     */
    @Step
    public void click_logout() {
        accountPage.click_logout_button();
    }

    /**
     * Open login page.
     * Validation: should see message "LOG IN".
     * @param text - message "LOG IN".
     */
    @Step
    public void should_see_valid_logout_text(String text) {
        homePagePage.click_login_page();
        Assert.assertEquals(loginPage.get_login_identificator(), text);
    }
}
