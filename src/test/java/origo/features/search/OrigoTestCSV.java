package origo.features.search;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import origo.steps.serenity.*;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/features/search/SearchData.csv")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrigoTestCSV {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public LoginSteps login;

    @Steps
    public LogoutSteps logout;

    @Steps
    public SearchSteps search;

    @Steps
    public AddToBagSteps addtoBag;

    String nonvalidTerm, errorMessage, validTerm, successMessage, email, pass;

    @Issue("#login-1")
    @Test
    public void a_validLogIntest() {
        //pornim de la home page
        login.is_the_home_page();
        login.click_login_page();
        login.should_see_valid_login_text("LOG IN");
        login.enters_data_and_click_login(email, pass);
        login.should_see_valid_account_text("Logged in as Puscasu Ana-Maria (Log out) • Addresses (0)");
    }


    @Test
    public void b_searching_by_nonvalid_keyword_should_display_the_error_message() {
        search.is_the_home_page();
        search.looks_for(nonvalidTerm);
        search.should_see_non_valid_text(errorMessage);
    }

    @Test
    public void c_searching_by_valid_keyword_should_display_the_corresponding_result() {
        search.is_the_home_page();
        search.looks_for(validTerm);
        search.should_see_valid_text(successMessage);
    }

    @Test
    public void d_should_correctly_add_to_bag() {
        addtoBag.is_the_home_page();
        addtoBag.go_shopping();
        addtoBag.should_see_valid_shop_text("SHOP");
        addtoBag.select_product_type();
        addtoBag.should_see_valid_product_category_text("CEAI");
        addtoBag.select_product("CEAI ALB SPIRULINA");
        addtoBag.should_see_valid_product_text("CEAI ALB SPIRULINA");
        addtoBag.set_units_set_quantity_and_click_collect("3", "1000");
        addtoBag.should_see_text("CART");
        addtoBag.should_find_correct_product_in_bag("Ceai Alb Spirulina");
        addtoBag.should_find_correct_units_in_bag("3");
    }

    @Test
    public void e_valid_login_logout_test() {
        login.is_the_home_page();
        login.click_login_page();
        login.should_see_valid_login_text("LOG IN");
        login.enters_data_and_click_login("puscasu.a.m@gmail.com", "testare");
        login.should_see_valid_account_text("Logged in as Puscasu Ana-Maria (Log out) • Addresses (0)");
        logout.click_logout();
        logout.click_login_page();
        logout.should_see_valid_logout_text("LOG IN");
    }

    @Ignore
    @Test
    public void f_all_tests(){
        //pornim de la home page
        login.is_the_home_page();
        login.click_login_page();
        login.should_see_valid_login_text("LOG IN");
        login.enters_data_and_click_login(email, pass);
        login.should_see_valid_account_text("Logged in as Puscasu Ana-Maria (Log out) • Addresses (0)");

        search.is_the_home_page();
        search.looks_for(nonvalidTerm);
        search.should_see_non_valid_text(errorMessage);

        search.is_the_home_page();
        search.looks_for(validTerm);
        search.should_see_valid_text(successMessage);

        addtoBag.is_the_home_page();
        addtoBag.go_shopping();
        addtoBag.should_see_valid_shop_text("SHOP");
        addtoBag.select_product_type();
        addtoBag.should_see_valid_product_category_text("CEAI");
        addtoBag.select_product("CEAI ALB SPIRULINA");
        addtoBag.should_see_valid_product_text("CEAI ALB SPIRULINA");
        addtoBag.set_units_set_quantity_and_click_collect("3", "1000");
        addtoBag.should_see_text("CART");
        addtoBag.should_find_correct_product_in_bag("Ceai Alb Spirulina");
        addtoBag.should_find_correct_units_in_bag("3");

        logout.click_login_page();
        logout.should_see_valid_login_text("Logged in as Puscasu Ana-Maria (Log out) • Addresses (0)");
        logout.click_logout();
        logout.click_login_page();
        logout.should_see_valid_logout_text("LOG IN");
    }
}
