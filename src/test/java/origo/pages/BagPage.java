package origo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class BagPage extends PageObject {

    /**
     * Get text from xpath.
     * @return bag success message.
     */
    public String get_bag_identificator() {
        return find(By.xpath("/html/body/div[3]/div[1]/h1")).getText();
    }

    /**
     * Get all elements from table.
     */
    @FindBy(xpath="/html/body/div[3]/div[2]/form/table")
    private WebElementFacade tableSelector;

    /**
     * Search a product by product name.
     * @param productName - searched term.
     * @return boolean result -  true for valid product.
     */
    public boolean check_element_by_attribute_value_product(String productName) {
//        List<WebElement> list = this.tableSelector.findElements(By.cssSelector("tbody > tr > td.cart-item-product.first > span.cart-item-title > a"));
        List<WebElement> list = this.tableSelector.findElements(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr/td[1]/span[2]/a"));
        for (int i = 0; i<list.size(); i++) {
            String name = list.get(i).getText();
            if (name.equalsIgnoreCase(productName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Search a product by input.
     * @param attribute - by value
     * @param units - units
     * @return boolean result true if units parameter = product units from bag
     */
    public boolean check_element_by_attribute_value_units(String attribute, String units) {
        List<WebElement> list = this.tableSelector.findElements(By.tagName("input"));
        for (int i = 0; i<list.size(); i++) {
            String buc = list.get(i).getAttribute(attribute);
            if (buc.equals(units)) {
                return true;
            }
        }
        return false;
    }
}
