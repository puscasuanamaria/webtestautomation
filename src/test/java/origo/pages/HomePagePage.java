package origo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://origocoffee.ro/")
public class HomePagePage extends PageObject {

    @FindBy(xpath ="/html/body/div[2]/header/nav/ul/li[5]/a")
    private WebElementFacade loginPageButton;

    @FindBy(name="q")
    private WebElementFacade searchInput;

    @FindBy(xpath = "//*[@id=\"slide-1\"]/a")
    private WebElementFacade visitShopButton;


    public void click_login_page() {
        loginPageButton.click();
    }

    public void click_search_box() {
        searchInput.click();
    }

    public void enters(String term) {
        searchInput.type(term);
    }

    public void starts_search() {
        searchInput.submit();
    }

    public void visit_shop() {
        visitShopButton.click();
    }
}
