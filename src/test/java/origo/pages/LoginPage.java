package origo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class LoginPage extends PageObject {

    @FindBy(name = "customer[email]")
    private WebElementFacade customerEmail;

    @FindBy(name = "customer[password]")
    private WebElementFacade customerPass;

    @FindBy(xpath = "//*[@id=\"customer_login\"]/div[3]/input")
    private WebElementFacade loginButton;

    /**
     * Get text from xpath
     * @return login identificatin
     */
    public String get_login_identificator() {
        return find(By.xpath("/html/body/div[3]/div[1]/h1")).getText();
    }

    /**
     * Enter login parameter.
     * @param email
     */
    public void enter_email(String email) {
        customerEmail.type(email);
    }

    /**
     * Enter login parameter.
     * @param pass
     */
    public void enter_password(String pass) {
        customerPass.type(pass);
    }

    /**
     * Click login button.
     */
    public void click_signin_button() {
        loginButton.click();
    }
}
