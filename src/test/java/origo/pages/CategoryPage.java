package origo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CategoryPage extends PageObject {

    @FindBy(xpath="/html/body/div[3]/div[2]/div[3]/div[1]/figure/a")
    private WebElementFacade productSelector;

    @FindBy(xpath="/html/body/div[3]/div[2]/div[3]")
    private WebElementFacade collectionSelector;

    /**
     * Get text from xpath.
     * @return product category  - success message.
     */
    public String get_category_identificator() {
        return find(By.xpath("/html/body/div[3]/div[1]/h1")).getText();
    }

    /**
     * Search product by product name.
     * Result: click on selected product.
     * @param productName
     */
    public void select_product(String productName) {
        List<WebElement> list = this.collectionSelector.findElements(By.xpath("/html/body/div[3]/div[2]/div[3]/div/div/h3/a"));

        for (int i = 0; i<list.size(); i++) {
            String name = list.get(i).getText();
            if (name.equalsIgnoreCase(productName)) {
                list.get(i).click();
                break;
            }
        }
    }
}
