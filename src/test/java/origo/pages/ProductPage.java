package origo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class ProductPage extends PageObject {

    @FindBy(name = "quantity")
    private WebElementFacade units;

    @FindBy(xpath = "//*[@id=\"product-variants-option-0\"]")
    private WebElementFacade quantity;

    @FindBy(xpath = "//*[@id=\"product-form\"]/div[2]/input")
    private WebElementFacade collectButton;

    public void set_units(String units) {
        this.units.clear();
        this.units.type(units);
    }

    public void click_quantity() {
        quantity.click();
    }


    public void select_quantity(String quantity) {
        // Option 1
        Select dropdown = new Select(this.quantity.findElement(By.id("product-variants-option-0")));
        dropdown.selectByVisibleText(quantity);

//        // Option 2
//        this.quantity.selectByVisibleText(quantity);
//        this.quantity.selectByValue(quantity).click();
    }

    public void click_collect() {
        collectButton.click();
    }

    public String get_product_identificator() {
        return find(By.xpath("/html/body/div[3]/div[2]/div[1]/div[2]/h1")).getText();
    }
}
