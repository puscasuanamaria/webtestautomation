package origo.steps.serenity;

import net.thucydides.core.annotations.Step;
import origo.pages.HomePagePage;
import origo.pages.SearchPage;

import static junit.framework.TestCase.assertTrue;

public class SearchSteps {
    HomePagePage homePagePage;
    SearchPage searchPage;

    /**
     * Open main page https://origocoffee.ro.
     */
    @Step
    public void is_the_home_page() {
        homePagePage.open();
    }

    /**
     * Search: click search box, enter searched term and submit.
     * @param term - to be searched
     */
    @Step
    public void looks_for(String term) {
        homePagePage.click_search_box();
        homePagePage.enters(term);
        homePagePage.starts_search();
    }

    /**
     * Open search page - with search result.
     * Function used for valid term.
     * Should see success message.
     * @param text
     */
    @Step
    public void should_see_valid_text(String text) {
        assertTrue(searchPage.get_valid_search_identificator().contains(text));
    }

    /**
     * Open search page - with search result.
     * Function used for non valid term.
     * Should see error message.
     * @param text
     */
    @Step
    public void should_see_non_valid_text(String text) {
        assertTrue(searchPage.get_non_valid_search_identificator().contains(text));
    }
}



