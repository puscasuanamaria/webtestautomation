package origo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class ShopPage extends PageObject {

    @FindBy(xpath="/html/body/div[3]/div[2]/div/article[2]")
    private WebElementFacade productIdentification;


    public void select_product_type() {
        productIdentification.click();
    }

    public String get_shop_identificator() {
        return find(By.xpath("/html/body/div[3]/div[1]/h1")).getText();
    }
}
