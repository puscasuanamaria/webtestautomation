package origo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.Collection;

public class SearchPage extends PageObject {

    @FindBy(xpath="/html/body/div[3]/div[2]/div/div[2]/div/figure")
    private WebElementFacade productIdentification;

    public String get_valid_search_identificator() {
        return find(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/h3/a")).getText();
    }

    public String get_non_valid_search_identificator() {
        return find(By.xpath("/html/body/div[3]/div[2]/div/div/p")).getText();
    }

    public void click_product() {
        find(By.cssSelector(".search-results-wrapper .product-list-item:first a:first")).click();
    }
}
